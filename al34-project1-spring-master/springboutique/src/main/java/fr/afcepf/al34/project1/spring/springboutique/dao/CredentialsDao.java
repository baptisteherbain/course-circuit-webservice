package fr.afcepf.al34.project1.spring.springboutique.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;

public interface CredentialsDao extends CrudRepository<Credentials, Integer> {
	
	Credentials findByLogin(String login);

}
