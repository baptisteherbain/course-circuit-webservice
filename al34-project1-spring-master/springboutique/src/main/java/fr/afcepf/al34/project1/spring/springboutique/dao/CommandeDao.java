package fr.afcepf.al34.project1.spring.springboutique.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Commande;

public interface CommandeDao  extends CrudRepository<Commande, Integer>{
	
	List<Commande> findAll();

}
