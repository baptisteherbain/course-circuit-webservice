package fr.afcepf.al34.project1.spring.springboutique.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import fr.afcepf.al34.project1.spring.springboutique.business.ClientService;
import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.business.UserService;
import fr.afcepf.al34.project1.spring.springboutique.dto.MessageDto;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.service.IMessageService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@SessionScoped
@Getter @Setter @NoArgsConstructor
public class MessageMBean {

	private Product currentProduct = null;
	private Client currentClient;
	private MessageDto messageDto = new MessageDto();
	private List<MessageDto> comments = new ArrayList<MessageDto>();
	private String idProduit = "";
	private String idClient = "";
	private String currentClientFirstName = "";
	private String currentClientLastName = "";
	private String message;
	private String mycomment;
	private Integer currentProductId;
	private Integer currentClientId;
	private Date date;
	private String dateDuJour;

	@Inject
	private IMessageService messageService;
	
	@Inject
	private ProductService productService;
	
	@Inject
	private ClientService clientService;

	@PostConstruct
	public void init() {
//		currentProductId = currentProduct.getId();
//		idProduit = "" + currentProductId;
//		comments = messageService.getMessageByIdProduit(idProduit);
//		messageDto = new MessageDto();
		System.out.println("init message managed bean");
	}

	//Get depuis le service messagerie pour récupérer les messages d'utilisateurs sur un produit dans la BDD du webservice
	public List<MessageDto> doGetComments() {
		
		currentProduct = (Product) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("currentProduct");
		currentProductId = currentProduct.getId();
		
		System.out.println("current product id : " + currentProductId );
		
		idProduit = "" + currentProductId;
		List<MessageDto> comments = messageService.getMessageByIdProduit(idProduit);
		
		System.out.println("doGetComments OK");
		
		return comments;
	}
	
	//Post vers le service messagerie pour enregistrer le message dans la BDD du webservice
	public String doAddComment() {
		
		System.out.println("Enter doAddComment");
		
		currentClient = (Client) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("currentClient");
		System.out.println("currentClient " + currentClient);
		
		currentClientId = currentClient.getId();
		System.out.println("currentClientId " + currentClientId);
		
		currentClientFirstName = currentClient.getFirstName();
		System.out.println("currentClientFirstName " + currentClientFirstName);
		currentClientLastName = currentClient.getLastName();
		System.out.println("currentClientLastName " + currentClientLastName);
		
		if(currentClientId != null) {
			idClient = "" + currentClientFirstName + " " + currentClientLastName;
			System.out.println(idClient);
		} else {
			idClient = "visiteur";
			System.out.println("idClient nulle");
		}
		
		currentProductId = currentProduct.getId();
		idProduit = "" + currentProductId;
		System.out.println("idProduit : " + idProduit);
		
		SimpleDateFormat formater = null;
		date = new Date();
		//formater = new SimpleDateFormat("dd MMMMM yyyy 'à' hh:mm a");
		formater = new SimpleDateFormat("dd MMMMM yyyy");
		dateDuJour = formater.format(date);
		System.out.println("date : " + dateDuJour);
		
		System.out.println("idMessage : " + messageDto.getIdMessage());
		
		messageDto.setIdMessage(messageDto.getIdMessage());
		messageDto.setIdClient(idClient);
		messageDto.setIdProduit(idProduit);
		messageDto.setTxtMessage(getMycomment());
		messageDto.setDateDuJour(formater.format(date));
		messageDto.setDate(date);
		System.out.println("messageDto : " + messageDto);
		
		messageService.postMessage(messageDto);
		System.out.println("messageDto a bien été enregistré dans la base du webservice Message");
		
		mycomment="";
		
		return null;
	}
	
}
