package fr.afcepf.al34.project1.spring.springboutique.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class MessageDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idMessage;
	
	private String txtMessage;
	private String idClient;
	private String idProduit;
	private Date date;
	private String dateDuJour;
	
	public MessageDto(long idMessage, String txtMessage, String idClient, String idProduit, Date date, String dateDuJour) {
		super();
		this.idMessage = idMessage;
		this.txtMessage = txtMessage;
		this.idClient = idClient;
		this.idProduit = idProduit;
		this.date = date;
		this.dateDuJour = dateDuJour;
	}


}
