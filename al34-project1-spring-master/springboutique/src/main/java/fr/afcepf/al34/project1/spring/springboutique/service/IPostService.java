package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Date;
import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.dto.PostDto;

public interface IPostService {

	PostDto getPostById(long idPost);
	List<PostDto> getPostsByIdClient(String idClient);
	List<PostDto> getPostsByIdProduit(String idProduit);
	List<PostDto> getPostsByDate(Date date);
	PostDto postPost(PostDto postDto);
	void deletePost(PostDto postDto);
	
}
