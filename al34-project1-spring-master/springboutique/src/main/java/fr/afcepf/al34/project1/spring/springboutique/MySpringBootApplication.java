package fr.afcepf.al34.project1.spring.springboutique;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import fr.afcepf.al34.project1.spring.springboutique.dto.ProductPersoDto;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.service.IProductPersoService;

@SpringBootApplication
public class MySpringBootApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {

		SpringApplication app = new SpringApplication(MySpringBootApplication.class);
		
		app.setAdditionalProfiles("initData","import.sql");
		ConfigurableApplicationContext context = app.run(args);
		System.out.println("http://localhost:8080/myJsfSpringBootApp");
		//testAppelWsRest(context);
	}

private static void testAppelWsRest(ApplicationContext contextSpring) {
	/*
	// variante 1 avec DeviseServiceDelegate sans @Service (pas vu comme composant Spring)
	DeviseServiceDelegate deviseServiceDelegate = new DeviseServiceDelegate();
	DeviseDto deviseEur = deviseServiceDelegate.getDeviseByCode("EUR");
	System.out.println("deviseEur="+deviseEur);
	*/
	
//	//variante 2 avec DeviseServiceDelegate comportant @Service (vu comme composant Spring)
//	IDeviseService deviseServiceDelegate = contextSpring.getBean(IDeviseService.class); //équivalent de @Autowired
//	DeviseDto deviseEur = deviseServiceDelegate.getDeviseByCode("EUR");
//	System.out.println("** deviseEur="+deviseEur);
//	
//	DeviseDto nouvelleDevise = new DeviseDto("sc", "Super Credit Intergalactique" , 0.01);
//	DeviseDto deviseSauvegardee = deviseServiceDelegate.postDevise(nouvelleDevise);
//	System.out.println("deviseSauvegardee="+deviseSauvegardee);
//	
//	List<DeviseDto> listeDevises = deviseServiceDelegate.getAllDevises();
//	System.out.println("listeDevises="+listeDevises);
//	
//	deviseServiceDelegate.deleteDevise("sc");
//	
//	//vérif suppression:
//	DeviseDto deviseScSupprimee = deviseServiceDelegate.getDeviseByCode("sc");
//	System.out.println("deviseScSupprimee="+deviseScSupprimee);
	IProductPersoService productPersoServiceDelegate = contextSpring.getBean(IProductPersoService.class);
	List<ProductPersoDto> productPersoAll = productPersoServiceDelegate.getAllProductPerso();
	
	System.out.println("liste de produit perso = "+productPersoAll);
	
	Category theCat = new Category(3,"Legging");
	System.out.println(theCat.toString());
	ProductPersoDto productPerso =new ProductPersoDto(2,4,"Short Bleu","Taille S",10.0,"Jeanne Eléou",theCat,true);
	ProductPersoDto producSave = productPersoServiceDelegate.postProductPerso(productPerso);
	List<ProductPersoDto> productPersobyIdClient = productPersoServiceDelegate.getProductPersoByIdClient(1);
	System.out.println("liste de produit perso par client = "+productPersobyIdClient);
	//	System.err.println(producSave);

	
	
}
}
