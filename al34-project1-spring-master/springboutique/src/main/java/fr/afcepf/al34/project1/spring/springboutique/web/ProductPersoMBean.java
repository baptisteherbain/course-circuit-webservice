package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.annotation.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fr.afcepf.al34.project1.spring.springboutique.business.CategoryService;
import fr.afcepf.al34.project1.spring.springboutique.dto.ProductPersoDto;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.service.ProductPersoServiceDelegate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Named("mbProductPerso")
@RequestScoped
@Getter @Setter @NoArgsConstructor
public class ProductPersoMBean {

	@Inject
	ProductPersoServiceDelegate productPersoService;
	@Inject
	CategoryService categoryService;
	
	private List<Category> categories;
	private List<ProductPersoDto> productPersoDtos;
	private int category;
	private ProductPersoDto ppd = new ProductPersoDto();
	private String message;
	
	
	@ManagedProperty( "#{catalogueMBean}")
	private CatalogueMBean mblogin;
	
	@PostConstruct
	public void init() {
		this.categories= categoryService.getAllCategories();
		this.productPersoDtos= productPersoService.getAllProductPerso();
	}
	
	public String getAll() {
		this.productPersoDtos = productPersoService.getAllProductPerso();
		return "resultSearchProductPerso.xhtml?faces-redirect=true";
	}
	public String doAddProductPerso() {
		
		CatalogueMBean m =(CatalogueMBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("catalogueMBean");
	
		
		ppd.setIdClient(m.getClient().getId());
		ppd.setVendu(false);
		ppd.setCategory(categories.get(category-1));
		productPersoService.postProductPerso(ppd);
		productPersoDtos= productPersoService.getAllProductPerso();
		
		message = "Votre article a bien été enregistré";
		return "resultSearchProductPerso.xhtml";
	}
	
}
