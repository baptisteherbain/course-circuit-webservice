package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.project1.spring.springboutique.dao.CommandeDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Commande;

public class CommandeServiceImpl implements CommandeService{

	@Autowired
	CommandeDao commandeDao;
	
	@Override
	public List<Commande> findAll() {
		return commandeDao.findAll();
	}

}
