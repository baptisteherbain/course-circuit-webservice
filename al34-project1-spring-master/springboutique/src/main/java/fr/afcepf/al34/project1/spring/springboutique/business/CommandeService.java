package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Commande;

public interface CommandeService {

	List<Commande> findAll();
}
