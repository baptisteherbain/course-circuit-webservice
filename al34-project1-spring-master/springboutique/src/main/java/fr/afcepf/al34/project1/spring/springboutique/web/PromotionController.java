package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.project1.spring.springboutique.business.PromotionService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Promotion;

@RestController //classe de WS REST avec spring MVC (cas particulier de @Component)
@RequestMapping(value="/api/promotion" , headers="Accept=application/json")
@CrossOrigin(origins = "*")
public class PromotionController {
	
	@Autowired
	private PromotionService promotionService;
	
	@GetMapping(value="")
    public List<Promotion> findAll(){
	//	System.out.println(productService.getAllProducts().toString());
//        return userService.getAllUser();
		return	promotionService.getAllPromotions();
    }
	

}
