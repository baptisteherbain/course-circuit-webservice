package fr.afcepf.al34.project1.spring.springboutique.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Type;

public interface TypeDao extends CrudRepository<Type, Integer> {
	@Query("SELECT t.color FROM Type t")
	List<String> findAllColors();
	@Query("SELECT t.size FROM Type t")
	List<String> findAllSizes();
}
