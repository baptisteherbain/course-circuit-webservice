package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "product")
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String name;

	private String description;

	private double price;

	public Product(String name, String description, double price, String image) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.image = image;
	}

	@Column(name = "technical_infos")
	private String technicalInfos;

	@Column(name = "qr_code")
	private String qrCode;

	@Column(name = "delivery_fees")
	private double deliveryFees;

	private double vat;

	private double cost;
	
	private String image;

	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Composition composition;

	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Category category;

	@OneToMany
	@JsonIgnore
	private List<Photo> photos = new ArrayList<Photo> ();

	@OneToMany (fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Inventory> inventories = new ArrayList<Inventory> ();

	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private CustomerGender customerGender;

	@Column(name = "customer_ages")
	@OneToMany
	@JsonIgnore
	private List<CustomerAge> customerAges = new ArrayList<CustomerAge> ();

	@OneToMany (mappedBy = "product")
	@JsonIgnore
	private List<SportProduct> sportProducts = new ArrayList<SportProduct> ();

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    @JsonIgnore
    private Promotion promotion;

	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Supplier supplier;

	@OneToMany(mappedBy = "product")
	@JsonIgnore
	private List<Rating> ratings = new ArrayList<Rating> ();


}
