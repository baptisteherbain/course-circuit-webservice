package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.dto.ProductPersoDto;

public interface IProductPersoService {

	ProductPersoDto getProductPersoByID(Integer Id, Integer IdClient);
	List<ProductPersoDto> getProductPersoByIdClient(Integer IdClient);
	List<ProductPersoDto> getAllProductPerso();
	ProductPersoDto postProductPerso(ProductPersoDto productPerso );
	void DeleteProductPerso(ProductPersoDto productPerso);
}
