package fr.afcepf.al34.project1.spring.springboutique.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString()
@Entity
public class PostDto {

	@Id
	@GeneratedValue(strategy  = GenerationType.AUTO)
	private long idPost;
	
	private String idClient;
	private String clientFirstName;
	private String clientLastName;
	private String idProduit;
	private String txtAvis;
	private Date dateAvis;
	private String dateDuJour;
	
	//Constructeur sans la date du jour
	public PostDto(long idMessage, String idClient, String clientFirstName, String clientLastName, String idProduit, String txtAvis,
			Date dateAvis) {
		super();
		this.idClient = idClient;
		this.clientFirstName = clientFirstName;
		this.clientLastName = clientLastName;
		this.idProduit = idProduit;
		this.txtAvis = txtAvis;
		this.dateAvis = dateAvis;
	}

	//Constructeur avec la date du jour
	public PostDto(long idMessage, String idClient, String clientFirstName, String clientLastName, String idProduit, String txtAvis,
			Date dateAvis, String dateDuJour) {
		super();
		this.idClient = idClient;
		this.clientFirstName = clientFirstName;
		this.clientLastName = clientLastName;
		this.idProduit = idProduit;
		this.txtAvis = txtAvis;
		this.dateAvis = dateAvis;
		this.dateDuJour = dateDuJour;
	}
	
}
