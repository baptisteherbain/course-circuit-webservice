package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.CompositionDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Composition;

@Service
@Transactional
public class CompositionServiceImpl implements CompositionService{

	@Autowired
	CompositionDao compositionDao;
	
	
	@Override
	public Composition saveInBase(Composition c) {
		return compositionDao.save(c);
	}
	
	@Override
	public List<Composition> getAllCompositions() {

		return compositionDao.findAll();
	}


	
}
