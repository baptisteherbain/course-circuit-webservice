package fr.afcepf.al34.project1.spring.springboutique.dto;

import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor 
public class ProductPersoDto {

	private Integer id;
	
	private Integer idClient;

	private String name;

	private String description;

	private double price;
	
	private String commentaire;
	
	private Category category;
	
	private boolean vendu;

	public ProductPersoDto(Integer id, Integer idClient, String name, String description, double price,
			String commentaire, Category category, boolean vendu) {
		super();
		this.id = id;
		this.idClient = idClient;
		this.name = name;
		this.description = description;
		this.price = price;
		this.commentaire = commentaire;
		this.category = category;
		this.vendu = vendu;
	}

	


	
}
