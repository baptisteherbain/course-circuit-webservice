package fr.afcepf.al34.project1.spring.springboutique.dto;

import java.util.Date;

import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString()
public class CommandDto {

	public Client client;
	public String country;
	public Product product;
	public Date date;


	public CommandDto(Client client, String country, Product product, Date date) {
		super();
		this.client = client;
		this.country = country;
		this.product = product;
		this.date = date;
	}

	


	
}
