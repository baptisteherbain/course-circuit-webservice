package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.dto.StockDto;

public interface IStockService {

	StockDto getStockByID(Integer Id, Integer IdProduit);
	StockDto getStockByIdProduit(Integer IdProduit);
	List<StockDto> getAllStock();
	void postStock(StockDto Stock);
	void changeStock(StockDto Stock);
	void DeleteStock(StockDto Stock);
}
