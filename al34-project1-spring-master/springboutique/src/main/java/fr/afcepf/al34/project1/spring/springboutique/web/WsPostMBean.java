package fr.afcepf.al34.project1.spring.springboutique.web;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.hibernate.mapping.Array;

import fr.afcepf.al34.project1.spring.springboutique.business.ClientService;
import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.dto.PostDto;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.service.PostServiceDelegate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.bytebuddy.utility.privilege.GetSystemPropertyAction;

@ManagedBean
@Getter @Setter @NoArgsConstructor
@SessionScoped
public class WsPostMBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Product currentProduct = null;
	private Client currentClient;
	private PostDto post = new PostDto();
	private List<PostDto> posts = new ArrayList<PostDto>();
	private String idProduit = "";
	private String idClient = "";
	private String currentClientFirstName = "";
	private String currentClientLastName = "";
	private String mypost;
	private Integer currentProductId;
	private Integer currentClientId;
	private Date date;
	private String dateDuJour;
	
	@Inject
	private ProductService productService;
	
	@Inject
	private ClientService clientService;
	
	@Inject
	private PostServiceDelegate postService;
	
	@PostConstruct
	public void init() {
		System.out.println("init post managed bean postconstruct method passed with success");
	}
	
	//Get depuis le service Post pour récupérer les messages d'utilisateurs sur un produit dans la BDD du webservice
	public List<PostDto> doGetPosts() {
		
		System.out.println("Enter doGetPosts method");
		
		currentProduct = (Product) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("currentProduct");
		currentProductId = currentProduct.getId();
		System.out.println("current product id = "+currentProductId);
		
		idProduit = "" + currentProductId;
		List<PostDto> posts = postService.getPostsByIdProduit(idProduit);
		
		System.out.println("doGetPosts succeed youpi");
		
		return posts;
	}
	
	public String doAddPost() {
		
		System.out.println("Enter doAddPost method");
		
		currentClient = (Client) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("currentClient");
		currentClientId = currentClient.getId();
		currentClientFirstName = currentClient.getFirstName();
		currentClientLastName = currentClient.getLastName();
		System.out.println("currentClientId = "+currentClientId);		
		System.out.println("currentClientFirstName = "+currentClientFirstName);
		System.out.println("currentClientLastName = "+currentClientLastName);
		
		if(currentClientId != null) {
			idClient = ""+currentClientFirstName+" "+currentClientLastName;
			System.out.println("Identité client = "+idClient);
		} else {
			idClient = "visiteur";
			System.out.println("Current client unidentified");
		}

		currentProductId = currentProduct.getId();
		idProduit = ""+currentProductId;
		System.out.println("idProduit = "+idProduit);
		
		SimpleDateFormat formater = null;
		date = new Date();
		formater = new SimpleDateFormat("dd MMMM yyyy");
		dateDuJour = formater.format(date);
		System.out.println("date = "+dateDuJour);
		
		System.out.println("idPost = "+post.getIdPost());
		
		post.setIdPost(post.getIdPost());
		post.setIdClient(idClient);
		post.setIdProduit(idProduit);
		post.setTxtAvis(getMypost());
		post.setDateDuJour(formater.format(date));
		post.setDateAvis(date);
		System.out.println("post = "+post);
		
		postService.postPost(post);
		System.out.println("post recorded in the webservice's database");
		
		mypost="";
		
		return null;
	}
}
