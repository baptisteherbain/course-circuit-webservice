package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.afcepf.al34.project1.spring.springboutique.dto.StockDto;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;

public class GestionClientServiceDelegate implements IGestionClient{
	
	ObjectMapper jsonMapper = new ObjectMapper();
	private RestTemplate restTemplate = new RestTemplate();
	
	@Value("${gestionclient.base-url}") //pour récupérer 
	private String baseUrl;

	@Override
	public List<Client> getAllClient() {
		// TODO Auto-generated method stub
		Client[] client = null;
		String url= baseUrl;
		client = restTemplate.getForObject(url, Client[].class);
		return Arrays.asList(client);
	}

}
