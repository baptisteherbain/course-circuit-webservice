package fr.afcepf.al34.project1.spring.springboutique.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WelcomePageRedirect implements WebMvcConfigurer {

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/")
        //.setViewName("forward:/index.html");
    	//.setViewName("forward:/welcome.html");
    	//.setViewName("forward:/welcomeV1.xhtml");
    	.setViewName("forward:/clientMain.xhtml");
    registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
  }
  
}
