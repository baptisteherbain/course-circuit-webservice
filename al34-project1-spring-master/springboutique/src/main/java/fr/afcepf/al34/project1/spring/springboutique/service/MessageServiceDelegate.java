package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.project1.spring.springboutique.dto.MessageDto;
import fr.afcepf.al34.project1.spring.springboutique.service.IMessageService;;

@Service
public class MessageServiceDelegate implements IMessageService {

	private RestTemplate restTemplate = new RestTemplate();
	
	@Value(value = "${message-api.base-url}")
	private String baseUrl;
	
	@Override
	public MessageDto getMessageById(String idMessage) {
		String url = baseUrl + "/id/" + idMessage;
		return restTemplate.getForObject(url, MessageDto.class);
	}

	@Override
	public List<MessageDto> getMessageByIdCLient(String idClient) {
		MessageDto[] tabMessages = null;
		String url = baseUrl + "/client/" + idClient;
		tabMessages = restTemplate.getForObject(url, MessageDto[].class);
		return Arrays.asList(tabMessages);
	}

	@Override
	public List<MessageDto> getMessageByIdProduit(String idProduit) {
		MessageDto[] tabMessages = null;
		String url = baseUrl + "/produit/" + idProduit;
		System.out.println(url);
		tabMessages = restTemplate.getForObject(url, MessageDto[].class);
		System.out.println(Arrays.asList(tabMessages));
		return Arrays.asList(tabMessages);
	}

	@Override
	public List<MessageDto> getMessageByDate(Date date) {
		MessageDto[] tabMessages = null;
		String url = baseUrl + "/dt/" + date;
		tabMessages = restTemplate.getForObject(url, MessageDto[].class);
		return Arrays.asList(tabMessages);
	}

	@Override
	public MessageDto postMessage(MessageDto messageDto) {
		String url = baseUrl + "/postMessage/";
		System.out.println(baseUrl);
		System.out.println(url);
		return restTemplate.postForObject(url, messageDto, MessageDto.class);
	}

	@Override
	public void deleteMessage(MessageDto messageDto) {
		String url = baseUrl;
		restTemplate.delete(url);		
	}

	
}
