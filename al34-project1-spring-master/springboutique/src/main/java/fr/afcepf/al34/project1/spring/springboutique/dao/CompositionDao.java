package fr.afcepf.al34.project1.spring.springboutique.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Composition;

public interface CompositionDao extends CrudRepository<Composition, Integer> {
	
	List<Composition> findAll();

}
