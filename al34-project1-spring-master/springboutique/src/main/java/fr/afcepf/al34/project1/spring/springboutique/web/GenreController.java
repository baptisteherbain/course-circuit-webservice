package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.project1.spring.springboutique.business.CustomerGenderService;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;


@RestController //classe de WS REST avec spring MVC (cas particulier de @Component)
@RequestMapping(value="/api/genre" , headers="Accept=application/json")
@CrossOrigin(origins = "*")
public class GenreController {

	@Autowired
	private CustomerGenderService genderService;
	
	@GetMapping(value="")
    public List<CustomerGender> findAll(){

		return	genderService.getAllCustomerGenders();
	}
}
