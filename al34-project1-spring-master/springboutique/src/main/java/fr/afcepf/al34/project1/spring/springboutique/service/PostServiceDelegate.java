package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.afcepf.al34.project1.spring.springboutique.dto.PostDto;

@Service
public class PostServiceDelegate implements IPostService{

	ObjectMapper jsonMapper = new ObjectMapper();

	private RestTemplate restTemplate = new RestTemplate();
	
	@Value(value = "${post-api.base-url}")
	private String baseUrl;
	
	@Override
	public PostDto getPostById(long idPost) {
		String url = baseUrl+idPost;
		System.out.println("getPostById url = "+url);
		return restTemplate.getForObject(url, PostDto.class);
	}

	@Override
	public List<PostDto> getPostsByIdClient(String idClient) {
		PostDto[] listPosts = null;
		String url = baseUrl+"/client/"+idClient;
		System.out.println("getPostsByIdClient url = "+url);
		listPosts = restTemplate.getForObject(url, PostDto[].class);
		System.out.println("listPosts par idClient = "+Arrays.asList(listPosts));
		return Arrays.asList(listPosts);
	}

	@Override
	public List<PostDto> getPostsByIdProduit(String idProduit) {
		PostDto[] listPosts = null;
		String url = baseUrl+"/produit/"+idProduit;
		System.out.println("getPostsByIdProduit url = "+url);
		listPosts = restTemplate.getForObject(url, PostDto[].class);
		System.out.println("listPosts par idProduit = "+Arrays.asList(listPosts));
		return Arrays.asList(listPosts);
	}

	@Override
	public List<PostDto> getPostsByDate(Date date) {
		PostDto[] listPosts = null;
		String url = baseUrl+"/dt/"+date;
		System.out.println("getPostsByDate url = "+url);
		listPosts = restTemplate.getForObject(url, PostDto[].class);
		System.out.println("listPosts par date = "+Arrays.asList(listPosts));
		return Arrays.asList(listPosts);
	}

	@Override
	public PostDto postPost(PostDto postDto) {
		String url = baseUrl+"/post/";
		System.out.println("post url = "+url);
		return restTemplate.postForObject(url, postDto, PostDto.class);
	}

	@Override
	public void deletePost(PostDto postDto) {
		String url = baseUrl;
		System.out.println("deletePost url = "+url);
		restTemplate.delete(url);
		
	}

}
