package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;

@RestController //classe de WS REST avec spring MVC (cas particulier de @Component)
@RequestMapping(value="/api/product" , headers="Accept=application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@GetMapping(value="")
    public List<Product> findAll(){
	//	System.out.println(productService.getAllProducts().toString());
//        return userService.getAllUser();
		return	productService.getAllProducts();
    }
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping(value="postProduct",  headers="Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Product postProduct(@RequestBody Product plop){
System.out.println("ws update" + plop);
		return	productService.saveInBase(plop);
    }
	@GetMapping(value="/id/{Id}")
    public Product getProductById(@PathVariable("Id") Integer Id){

		return	productService.findWithId(Id);
    }
}
