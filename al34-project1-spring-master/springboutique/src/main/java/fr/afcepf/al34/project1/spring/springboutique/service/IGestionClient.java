package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Client;

public interface IGestionClient {
 List<Client> getAllClient();
}
