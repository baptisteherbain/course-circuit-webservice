package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import fr.afcepf.al34.project1.spring.springboutique.business.UserService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value="/api/user")
public class ClientController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping(value="")
    public String findAll(){
		System.out.println(userService.getAllUsers().toString());
//        return userService.getAllUser();
		return	"{id: 123}";
    }

}
