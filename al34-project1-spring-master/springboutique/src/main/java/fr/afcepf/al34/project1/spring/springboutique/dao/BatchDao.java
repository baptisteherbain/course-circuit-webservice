package fr.afcepf.al34.project1.spring.springboutique.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Batch;
import fr.afcepf.al34.project1.spring.springboutique.entity.Cart;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;

public interface BatchDao extends CrudRepository<Batch, Integer> {

	@Query("SELECT b.product FROM Batch b WHERE b.cart = ?1")
	List<Product> findWithCart(Cart cart);
	
	
}
