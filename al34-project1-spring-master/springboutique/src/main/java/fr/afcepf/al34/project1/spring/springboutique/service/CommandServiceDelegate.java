package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.afcepf.al34.project1.spring.springboutique.dto.CommandDto;
import fr.afcepf.al34.project1.spring.springboutique.dto.StockDto;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;

@Service
public class CommandServiceDelegate implements ICommandService{

	ObjectMapper jsonMapper = new ObjectMapper();
	private RestTemplate restTemplate = new RestTemplate();
	
	@Value("${command-api.base-url}") //pour récupérer 
	private String baseUrl;

	@Override
	public CommandDto getCommandByID(Integer Id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommandDto getCommandByIdClient(Integer IdClient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CommandDto> getAllCommand() {
		CommandDto[] tabCommand = null;
		String url= baseUrl;
		tabCommand = restTemplate.getForObject(url, CommandDto[].class);
		return Arrays.asList(tabCommand);
	}

	@Override
	public void postCommand(CommandDto Command) {
		String url = baseUrl+"/";
		 restTemplate.postForObject(url,Command,CommandDto.class);
	}

	@Override
	public void DeleteCommand(CommandDto Command) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postProduct(Product product) {
		String url = baseUrl+"/";
		 restTemplate.postForObject(url,product,Product.class);
		
	}

	@Override
	public void postClient(Client client) {
		String url = baseUrl+"/";
		 restTemplate.postForObject(url,client,Client.class);
		
	}
}
