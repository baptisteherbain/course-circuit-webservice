package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.dto.CommandDto;
import fr.afcepf.al34.project1.spring.springboutique.dto.StockDto;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;

public interface ICommandService {

	CommandDto getCommandByID(Integer Id);
	CommandDto getCommandByIdClient(Integer IdClient);
	List<CommandDto> getAllCommand();
	void postCommand(CommandDto command);
	void postProduct(Product product);
	void postClient(Client client);
	void DeleteCommand(CommandDto Command);
}
