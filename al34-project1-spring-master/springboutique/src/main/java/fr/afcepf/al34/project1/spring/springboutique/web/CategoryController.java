package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.project1.spring.springboutique.business.CategoryService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;

@RestController //classe de WS REST avec spring MVC (cas particulier de @Component)
@RequestMapping(value="/api/category" , headers="Accept=application/json")
@CrossOrigin(origins = "*")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	
	@GetMapping(value="")
    public List<Category> findAll(){
		return categoryService.getAllCategories();
	}
	
}
