package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Date;
import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.dto.MessageDto;

public interface IMessageService {

	MessageDto getMessageById(String idMessage);
	List<MessageDto> getMessageByIdCLient(String idClient);
	List<MessageDto> getMessageByIdProduit(String idProduit);
	List<MessageDto> getMessageByDate(Date date);
	MessageDto postMessage(MessageDto messageDto);
	void deleteMessage(MessageDto messageDto);
}
