package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.project1.spring.springboutique.business.CompositionService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Composition;

@RestController //classe de WS REST avec spring MVC (cas particulier de @Component)
@RequestMapping(value="/api/composition" , headers="Accept=application/json")
@CrossOrigin(origins = "*")
public class CompositionController {
	
	@Autowired
	private CompositionService compositionservice;
	
	@GetMapping(value="")
    public List<Composition> findAll(){
		
		return compositionservice.getAllCompositions();
	}

}
