INSERT INTO `gender` (`id`, `name`) VALUES (1, 'Monsieur');
INSERT INTO `gender` (`id`, `name`) VALUES (2, 'Madame');

INSERT INTO `customer_gender` (`id`, `name`) VALUES (1, 'Homme');
INSERT INTO `customer_gender` (`id`, `name`) VALUES (2, 'Femme');

INSERT INTO `category` (`id`, `name`) VALUES (1, 'T-shirt');
INSERT INTO `category` (`id`, `name`) VALUES (2, 'Pull');
INSERT INTO `category` (`id`, `name`) VALUES (3, 'Legging');
INSERT INTO `category` (`id`, `name`) VALUES (4, 'Jogging');
INSERT INTO `category` (`id`, `name`) VALUES (5, 'Chaussure');
INSERT INTO `category` (`id`, `name`) VALUES (6, 'Pantalon');
INSERT INTO `category` (`id`, `name`) VALUES (7, 'Equipement');
INSERT INTO `category` (`id`, `name`) VALUES (8, 'Accessoire');
INSERT INTO `category` (`id`, `name`) VALUES (9, 'Manteaux');

INSERT INTO `type` (`id`, `size`, `color`) VALUES (1, 'XXL', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (2, 'XL', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (3, 'L', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (4, 'M', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (5, 'S', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (6, '14 ans', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (7, '12 ans', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (8, '10 ans', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (9, '8 ans', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (10, '37', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (11, '38', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (12, '39', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (13, '40', 'Bleu');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (14, '41', 'Bleu');

INSERT INTO `type` (`id`, `size`, `color`) VALUES (15, 'XXL', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (16, 'XL', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (17, 'L', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (18, 'M', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (19, 'S', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (20, '14 ans', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (21, '12 ans', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (22, '10 ans', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (23, '8 ans', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (24, '37', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (25, '38', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (26, '39', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (27, '40', 'Rouge');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (28, '41', 'Rouge');

INSERT INTO `type` (`id`, `size`, `color`) VALUES (29, 'XXL', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (30, 'XL', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (31, 'L', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (32, 'M', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (33, 'S', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (34, '14 ans', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (35, '12 ans', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (36, '10 ans', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (37, '8 ans', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (38, '37', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (39, '38', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (40, '39', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (41, '40', 'Noir');
INSERT INTO `type` (`id`, `size`, `color`) VALUES (42, '41', 'Noir');

INSERT INTO `customer_age` (`id`, `name`) VALUES (1, 'Kids');
INSERT INTO `customer_age` (`id`, `name`) VALUES (2, 'Adulte');
INSERT INTO `customer_age` (`id`, `name`) VALUES (3, 'Senior');

INSERT INTO `sport` (`id`, `name`) VALUES (1, 'Ski');
INSERT INTO `sport` (`id`, `name`) VALUES (2, 'Surf');
INSERT INTO `sport` (`id`, `name`) VALUES (3, 'Rando');
INSERT INTO `sport` (`id`, `name`) VALUES (4, 'Fitness');
INSERT INTO `sport` (`id`, `name`) VALUES (5, 'Trek');
INSERT INTO `sport` (`id`, `name`) VALUES (6, 'Equitation');
INSERT INTO `sport` (`id`, `name`) VALUES (7, 'Pêche');
INSERT INTO `sport` (`id`, `name`) VALUES (8, 'Tennis');
INSERT INTO `sport` (`id`, `name`) VALUES (9, 'Basket');
INSERT INTO `sport` (`id`, `name`) VALUES (10, 'Chasse');

INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (1, true,'Coton', 80);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (2, false,'Polyester', 90);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (3, false,'Polyamide', 75);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (4, false,'Élasthanne', 85);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (5, false,'Lycra', 95);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (6, true,'Duvet', 30);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (7, true,'Plume', 40);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (8, false,'Gore-Tex', 60);
INSERT INTO `composition` (`id`, `is_natural`, `name`, `quantity`) VALUES (9, true,'Laine', 60);

INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`,`image`, `name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (1, 2.0,3.0,'Notre équipe de Trekker a imaginé ce bonnet en laine Merinos pour vous protéger du froid lors des bivouacs en trekking. La laine mérinos, quand vous ne voulez pas choisir entre chaleur et compacité ! ','bonnet1.jpg','Bonnet de trekking montagne',10,'QRCODE1',' ',3.0,8,9,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`,`image`, `name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (2, 2.0,3.0,'Nous, surfeuses de tous niveaux, avons conçu ce maillot de bain pour la surfeuse experte qui surfe tous types de vagues. ','mdb1.jpg','Maillot de bain de surf 1 pièce',14,'QRCODE1',' ',3.0,2,2,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (3, 2.0,3.0,'Permet de débuter la pratique du tennis en toute confiance, avec un produit adapté aux contraintes de ce sport. Vendues en lot de 3 paires.','chaussette1.jpg','Chaussettes',3,'QRCODE1','Au ni',3.0,8,9,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (4, 2.0,3.0,'Vous craignez le froid ? Grâce à son duvet et à sa membrane, cette veste associe chaleur et imperméabilité, pour performer en ski quel que soit le temps','vesteski1.jpg','Veste de ski',120,'QRCODE1',' ',3.0,9,6,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (5, 2.0,3.0,'Conçues pour toutes les pratiques de chasse sur terrain accidenté ainsi quen montagne.','chaussureH1.jpg','Chaussure de chasse',280,'QRCODE1',' ',3.0,5,8,1,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (6, 2.0,3.0,'Plus légère et plus souple, la MT2, digne successeur de la MT, vous accompagne dans vos évasions par tous temps et sur terrains accidentés.','chaussureH4.jpg','Chaussure de trail',90,'QRCODE1',' ',3.0,5,8,1,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (7, 2.0,3.0,'Ces chaussures de randonnée montagne disposent de l’essentiel pour évoluer confortablement grâce à leur amorti intégral, leur pare-pierres protecteur, leur bon maintien et leur membrane imperméable! ','chaussureH6.jpg','Chaussure de randonnée',50,'QRCODE1',' ',3.0,5,8,1,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (8, 2.0,3.0,'Pantalon chaud et ventilé, idéal pour vos randonnée en hiver. A pied ou en raquettes, protection garantie avec ses guêtres anti-intrusion.','pant1.jpg','Pantalon chaud de randonnée',65,'QRCODE1','  ',3.0,6,7,1,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`,`image`, `name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (9, 2.0,3.0,'La pluie ne vous arrêtera pas grâce à cette chaussure de marche et son tissu déperlant.','chaussureF1.jpg','Chaussure de marche',20,'QRCODE1','  ',3.0,5,5,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`,`image`, `name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (10, 2.0,3.0,'Permet de débuter la pratique du tennis en toute confiance, avec un produit adapté aux contraintes de ce sport. Vendues en lot de 3 paires.','chaussette2.jpg','Chaussettes',3,'QRCODE1',' ',3.0,8,9,1,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (11, 2.0,3.0,'Notre crew de concepteurs a conçu ce débardeur pour la danseuse fitness qui souhaite des bretelles fines féminines qui ne bougent pendant sa pratique.','tshirtF1.jpg','Débardeur de danse',6,'QRCODE1',' ',3.0,1,2,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (12, 2.0,3.0,'Ce Tee Shirt respirant & résistant proposé en 5 couleurs pour une durée de 4 ans vous permettra de le personnaliser avec votre logo','tshirtF3.jpg','Tee-shirt atlétisme',5,'QRCODE1',' ',3.0,1,2,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (13, 2.0,3.0,'Vous êtes à la recherche dun T-Shirt doux et confortable ? Choisissez le T-Shirt 100% coton PUMA pour la pratique de la gym et du pilates', 'tshirtF4.jpg','Tee-shirt Puma',20,'QRCODE1',' ',3.0,1,2,2,null,null);

INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (14, 2.0,3.0,'Nos concepteurs backpackers ont conçu ce tee-shirt pour vous permettre de parcourir le monde en toute sérénité et dans tous les environnements.', 'tshirtH5.jpg','Tee-shirt manches longues Travel 500 Wool ',14,'QRCODE1',' ',3.0,1,9,1,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (15, 2.0,3.0,'Nos concepteurs ont imaginé ce pull coupe vent laine pour toutes les pratiques de chasse et de bushcraft par temps froid et venteux.', 'PullH1.jpg','pull chasse 900 laine coupe vent ',69,'QRCODE1',' ',3.0,2,9,1,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (16, 2.0,3.0,'Notre équipe de passionnés a développé ce legging pour la danseuse de modern jazz et contemporain tous niveaux qui souhaite mettre en valeur sa silhouette.', 'LeggingF1.jpg','Legging danse moderne noir sans couture ',10,'QRCODE1',' ',3.0,3,3,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (17, 2.0,3.0,'Cette chaussure de running femme a été conçue pour accompagner les coureuses à foulée neutre sur toutes distances lors de leurs sorties course à pied.', 'chaussf1.jpg','chaussure de running femme asics gel ziruss bleue ',130,'QRCODE1',' ',3.0,5,null,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (18, 2.0,3.0,'Notre motivation ? Vous proposer des chaussures confortables et légères qui vous garantissent une bonne adhérence sur tous sentiers naturels avec peu de dénivelé. Le tout à un prix très accessible .', 'chaussf2.jpg','chaussure de randonée nature ',21,'QRCODE1',' ',3.0,5,null,2,null,null);
INSERT INTO `product` (`id`,`cost`, `delivery_fees`, `description`, `image`,`name`, `price`,`qr_code`,`technical_infos`, `vat`,`category_id`, `composition_id`, `customer_gender_id`, `promotion_id`,`supplier_id`) VALUES (19, 2.0,3.0,'Cette chaussure convient pour vos sorties de marche sportive.', 'chaussf3.jpg','Chaussure de marche sportive Femme Flex Appeal ',60,'QRCODE1',' ',3.0,5,null,2,null,null);

INSERT INTO `credentials` (`id`, `hashed_password`, `login`,`salt` ) VALUES (1, 'plop', 'jpetou', 'jpetou');
INSERT INTO `credentials` (`id`, `hashed_password`, `login`,`salt` ) VALUES (2, 'plop', 'mcharpentier', 'mcharpentier');
INSERT INTO `credentials` (`id`, `hashed_password`, `login`,`salt` ) VALUES (3, 'plop', 'lsmith', 'lsmith');
INSERT INTO `credentials` (`id`, `hashed_password`, `login`,`salt` ) VALUES (4, 'plop', 'mmartin', 'mmartin');


INSERT INTO `user` (`type`,`id`,`first_name`, `last_name`, `total_connection_time`, `email`, `phone_number`,`end_date`,`start_date`, `credentials_id`,`gender_id`) VALUES (1, 1,'John','Pétou ',62,'jpetou@mail.fr','0142556674',null,null,1,1);
INSERT INTO `user` (`type`,`id`,`first_name`, `last_name`, `total_connection_time`, `email`, `phone_number`,`end_date`,`start_date`, `credentials_id`,`gender_id`) VALUES (1, 2,'Mathieu','Charpentier ',12,'mcharpentier@mail.fr','0142566674',null,null,2,1);
INSERT INTO `user` (`type`,`id`,`first_name`, `last_name`, `total_connection_time`, `email`, `phone_number`,`end_date`,`start_date`, `credentials_id`,`gender_id`) VALUES (2, 3,'Léa','Smith ',35,'lsmith@mail.fr','0142136674',null,null,3,2);
INSERT INTO `user` (`type`,`id`,`first_name`, `last_name`, `total_connection_time`, `email`, `phone_number`,`end_date`,`start_date`, `credentials_id`,`gender_id`) VALUES (2, 4,'Marie','Martin ',25,'mmartin@mail.fr','0142526674',null,null,4,2);




