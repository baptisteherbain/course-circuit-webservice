package fr.afcepf.al34.springboutique.service;

import java.util.Date;
import java.util.List;

import fr.afcepf.al34.springboutique.entity.Message;

public interface MessageService {

	Message rechercheMessageParIdMessage(long idMessage);
	
	List<Message> rechercheMessageParIdClient(Integer idClient);

	List<Message> rechercheMessageParIdProduit(Integer idProduit);
	
	List<Message> rechercheParDate(Date date);
	
	Message sauvegarderMessage(Message message);
	
	List<Message> findAll ();
	void delete (Integer id);
	
	void delete (Message message);
	
}
