package fr.afcepf.al34.springboutique.rest;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.springboutique.entity.Message;
import fr.afcepf.al34.springboutique.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/message-api/public/message", headers="Accept=application/json")
@Api(tags = { "Message REST-api"})
@CrossOrigin("*")
public class MessageRestCtrl {

	@Autowired
	private MessageService messageService;
	
	@ApiOperation(value = "getMessageById")
	@GetMapping(value = "/id/{idMessage")
	public Message getMessageById(@PathVariable("idMessage") long idMessage) {
		return messageService.rechercheMessageParIdMessage(idMessage);
	}

	@ApiOperation(value = "getMessageByIdClient")
	@GetMapping(value = "/client/{idClient}")
	public List<Message> getMessageByIdClient(@PathVariable("idClient") Integer idClient) {
		return messageService.rechercheMessageParIdClient(idClient);
	}

	@ApiOperation(value = "getMessageByIdProduit")
	@GetMapping(value = "/produit/{idProduit}")
	public List<Message> getMessageByIdProduit(@PathVariable("idProduit") Integer idProduit) {
		return messageService.rechercheMessageParIdProduit(idProduit);
	}

	@ApiOperation(value = "getMessageByDate")
	@GetMapping(value = "/dt/{Date}")
	public List<Message> getMessageByDate(@PathVariable("Date") Date date) {
		return messageService.rechercheParDate(date);
	}
	
	@PostMapping(value = "/postMessage" )
	public Message postMessage(@Valid @RequestBody Message message) {
		return messageService.sauvegarderMessage(message);
	}
	
	//en mode DELETE, url=http://localhost:8484/myJsfSpringBootApp/message-api/public/message/JPY
	@DeleteMapping(value = "/delete")
	public ResponseEntity<?> deleteMessage(@Valid @RequestBody Message message) {
		try {
			messageService.delete(message);
			return new ResponseEntity<String>("message supprimé", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	//en mode DELETE, url=http://localhost:8484/myJsfSpringBootApp/message-api/public/message
//	@DeleteMapping(value = "/delete")
//	public ResponseEntity<?> deleteMessage(@Valid @RequestBody Integer message) {
//		try {
//			
//			messageService.delete(message);
//			System.out.println("bouyya gooo");
//			return new ResponseEntity<String>("message supprimé", HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
	
	@DeleteMapping(value="delete/{id}")
	public void deleteCompteByNum(@PathVariable("id") Integer id) {
			messageService.delete(id);
			System.out.println("delllleeeete");
	}
	
	
	
		
}
