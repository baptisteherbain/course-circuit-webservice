package fr.afcepf.al34.springboutique.init;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.afcepf.al34.springboutique.entity.Message;
import fr.afcepf.al34.springboutique.service.MessageService;

@Profile("initData")
@Component
public class InitDataSet {

	@Autowired
	private MessageService messageService;
	
	@PostConstruct()
	public void initData() {
		Date date = new Date();
	//	Message messageTest = messageService.sauvegarderMessage(new Message(1, "txtMessage", "idClient", "idProduit", date, null));
		Message messageTest10 = messageService.sauvegarderMessage(new Message(10,"J'en suis très content",1,4, date, null));
		Message messageTest11 = messageService.sauvegarderMessage(new Message(11,"Rappelle moi au 068574210",2,4,date, null));	
		Message messageTest12 = messageService.sauvegarderMessage(new Message(12,"Vous êtes génials",3,4,date, null));	
		Message messageTest13 = messageService.sauvegarderMessage(new Message(13,"Je suis pas fan",1,5, date, null));
		Message messageTest14 = messageService.sauvegarderMessage(new Message(14,"Tu peux pas test",2,5,date, null));	
		Message messageTest15 = messageService.sauvegarderMessage(new Message(15,"Wesh grow",3,5,date, null));	
		Message messageTest16 = messageService.sauvegarderMessage(new Message(16,"Je l'utilise tous les jours",4,6, date, null));
		Message messageTest17 = messageService.sauvegarderMessage(new Message(17,"Y zoret pue myeu fér",2,6,date, null));	
		Message messageTest18 = messageService.sauvegarderMessage(new Message(18,"Sympa en orange",3,6,date, null));	
		Message messageTest19 = messageService.sauvegarderMessage(new Message(19,"J'ai pas essayé c'était un cadeau pour ma cousine",1,7, date, null));
		Message messageTest20 = messageService.sauvegarderMessage(new Message(20,"l’erreur 404 est un code d’erreur du protocole de communication HTTP sur le réseau Internet",2,7,date, null));	
		Message messageTest21 = messageService.sauvegarderMessage(new Message(21,"Ah ! Non ! C'est un peu court, jeune homme ! On pouvait dire... oh ! Dieu ! ... bien des choses en somme...",3,7,date, null));	
		Message messageTest22 = messageService.sauvegarderMessage(new Message(22,"Et en bleu c'est possible ?",1,8, date, null));
		Message messageTest23 = messageService.sauvegarderMessage(new Message(23,"oui",2,8,date, null));	
		Message messageTest24 = messageService.sauvegarderMessage(new Message(24,"en fait nan",3,8,date, null));	
		Message messageTest25 = messageService.sauvegarderMessage(new Message(25,"Je croyais que c'était en coton",1,9, date, null));
		Message messageTest26 = messageService.sauvegarderMessage(new Message(26,"Il manquait la page 42",2,9,date, null));	
		Message messageTest27 = messageService.sauvegarderMessage(new Message(27,"Trois fois neuf",3,9,date, null));	
		Message messageTest28 = messageService.sauvegarderMessage(new Message(28,"J'aime bien",1,10, date, null));
		Message messageTest29 = messageService.sauvegarderMessage(new Message(29,"Moi aussi",2,10,date, null));	
		Message messageTest30 = messageService.sauvegarderMessage(new Message(30,"J'aurais préféré en turquoise",3,10,date, null));	
		Message messageTest31 = messageService.sauvegarderMessage(new Message(31,"C'est un pantalon très confortable",1,11, date, null));
		Message messageTest32 = messageService.sauvegarderMessage(new Message(32,"Je l'ai acheté pour me déguiser en Indiana Jones et c'est très joli",2,11,date, null));	
		Message messageTest33 = messageService.sauvegarderMessage(new Message(33,"The best pants ever made !!!",3,11,date, null));	
		Message messageTest34 = messageService.sauvegarderMessage(new Message(34, "C'est très confortable", 1, 1, date, null));
		Message messageTest35 = messageService.sauvegarderMessage(new Message(35,"C'est super",1,1, date, null));
		Message messageTest36 = messageService.sauvegarderMessage(new Message(36,"Pas mal",2,1,date, null));	
		Message messageTest37 = messageService.sauvegarderMessage(new Message(37,"C'est un beau produit",3,1,date, null));	
		Message messageTest38 = messageService.sauvegarderMessage(new Message(38,"Chouette",1,2, date, null));
		Message messageTest39 = messageService.sauvegarderMessage(new Message(39,"Peut mieux faire",2,2,date, null));	
		Message messageTest40 = messageService.sauvegarderMessage(new Message(40,"C'est un peu tchip",3,2,date, null));	
		Message messageTest41 = messageService.sauvegarderMessage(new Message(41,"C'est chimique donc ce n'est pas naturel",1,3, date, null));
		Message messageTest42 = messageService.sauvegarderMessage(new Message(42,"Pas top",2,3,date, null));	
		Message messageTest43 = messageService.sauvegarderMessage(new Message(43,"C'est très cher pour ce que c'est",3,3,date, null));	
		
		
		System.out.println("InitData chargé");
		
	}
}
