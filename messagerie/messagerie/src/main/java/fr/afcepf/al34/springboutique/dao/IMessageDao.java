package fr.afcepf.al34.springboutique.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.springboutique.entity.Message;

public interface IMessageDao extends CrudRepository<Message, Integer> {

	Message findByIdMessage(long idMessage);
	
	List<Message> findByIdClient(Integer idClient);
	
	List<Message> findByIdProduit(Integer idProduit);
	
	List<Message> findByDate(Date date);
	List<Message> findAll();
	//void delete (Integer id);
	
}
