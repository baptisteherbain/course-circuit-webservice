package fr.afcepf.al34.springboutique.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.springboutique.entity.Message;
import fr.afcepf.al34.springboutique.service.MessageService;

@RestController //classe de WS REST avec spring MVC (cas particulier de @Component)
@RequestMapping(value="/message-api/public/message" , headers="Accept=application/json")
@CrossOrigin(origins = "*")
public class MessageController {
	
	@Autowired
	private MessageService messageService;
	
	@GetMapping(value="")
	public List<Message> findAll(){
		
		return messageService.findAll();
	}

}
