package fr.afcepf.al34.springboutique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MessagerieApplication extends SpringBootServletInitializer  {
	
	public static void main(String[] args) {
		SpringApplication messApp = new SpringApplication(MessagerieApplication.class);
		messApp.setAdditionalProfiles("initData");
		ConfigurableApplicationContext context = messApp.run(args);
		System.out.println("http://localhost:8484/messagerie/message-api/public/message");
	}
	

}
