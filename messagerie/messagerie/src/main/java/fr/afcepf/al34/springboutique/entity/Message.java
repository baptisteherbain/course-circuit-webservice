package fr.afcepf.al34.springboutique.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString
@Entity
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idMessage;
	
	private String txtMessage;
	
	private Integer idClient;
	
	private Integer idProduit;
		
	private Date date;
	
	private String dateDuJour;

	public Message(Integer idMessage, String txtMessage, int i, int j, Date date, String dateDuJour) {
		super();
		this.idMessage = idMessage;
		this.txtMessage = txtMessage;
		this.idClient = i;
		this.idProduit = j;
		this.date = date;
		this.dateDuJour = dateDuJour;
	}
	
	

}
