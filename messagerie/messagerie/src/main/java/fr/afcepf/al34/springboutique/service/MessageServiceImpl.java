package fr.afcepf.al34.springboutique.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.springboutique.dao.IMessageDao;
import fr.afcepf.al34.springboutique.entity.Message;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

	@Autowired
	private IMessageDao messageDao;
	
	@Override
	public Message rechercheMessageParIdMessage(long idMessage) {
		return messageDao.findByIdMessage(idMessage);
	}

	@Override
	public List<Message> rechercheMessageParIdClient(Integer idClient) {
		return messageDao.findByIdClient(idClient);
	}

	@Override
	public List<Message> rechercheMessageParIdProduit(Integer idProduit) {
		return messageDao.findByIdProduit(idProduit);
	}

	@Override
	public List<Message> rechercheParDate(Date date) {
		return messageDao.findByDate(date);
	}

	@Override
	public Message sauvegarderMessage(Message message) {
		return messageDao.save(message);
	}
	@Override
	public List<Message> findAll() {
		return messageDao.findAll();
	}
	@Override
	public void delete(Integer id) {
		 messageDao.deleteById(id);
	}
	//plop

	@Override
	public void delete(Message message) {
		// TODO Auto-generated method stub
		
	}

}
