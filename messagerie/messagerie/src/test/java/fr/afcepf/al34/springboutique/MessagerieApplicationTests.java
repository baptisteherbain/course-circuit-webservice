package fr.afcepf.al34.springboutique;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.afcepf.al34.springboutique.entity.Message;
import fr.afcepf.al34.springboutique.service.MessageService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MessagerieApplication.class})
class MessagerieApplicationTests {

	@Autowired
	private MessageService messageService;
	
	@Test
	public void contextLoads() {
	}

}
