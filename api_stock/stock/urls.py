from django.urls import path, include
from . import views 
from . import sql 
from rest_framework import routers 

router = routers.DefaultRouter()
router.register('stock', views.StockView)

urlpatterns = [
    path('', include(router.urls)),
    path('stock/change/<int:idProduit>/', views.change2),
]
