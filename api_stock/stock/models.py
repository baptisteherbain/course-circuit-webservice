from django.db import models

class Stock(models.Model):
	#id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
    idProduit= models.IntegerField(auto_created=False, primary_key=True, serialize=True, verbose_name='ID')
    quantity= models.IntegerField()
    quantityMin= models.IntegerField()
    price= models.FloatField()
    cost= models.FloatField()

    def __str__(self):
        return self.name
    class Meta:
        db_table = "stock"