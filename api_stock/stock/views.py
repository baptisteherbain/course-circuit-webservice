from django.http import HttpResponse
from django.db import connection
from rest_framework import viewsets
from .models import Stock
from .serializers import StockSerializer
from django.views.decorators.csrf import csrf_exempt
import json

class StockView(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer
    
@csrf_exempt
def change2(request, idProduit):
    parsed_json = (json.loads(request.body))
    idProduitFromJson=parsed_json["idProduit"]
    quantityFromJson=parsed_json["quantity"]
    with connection.cursor() as cursor:
        sql = "UPDATE Stock SET quantity = %s WHERE idProduit = %s"
        val = (quantityFromJson, idProduitFromJson)
        cursor.execute(sql, val)
    print("http://localhost:8888/stock/"+str(idProduit)+"/")
    return HttpResponse(request)