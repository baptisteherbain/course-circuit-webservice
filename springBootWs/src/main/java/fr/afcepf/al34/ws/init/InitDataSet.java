package fr.afcepf.al34.ws.init;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.afcepf.al34.ws.dao.ProductPersoDao;
import fr.afcepf.al34.ws.entity.Category;

import fr.afcepf.al34.ws.entity.ProductPerso;
import fr.afcepf.al34.ws.service.CategoryService;

import fr.afcepf.al34.ws.service.ProductPersoService;

@Profile("initDataProduct")//en développement seulement , pas en production 
@Component
public class InitDataSet {
	
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductPersoService productPersoService;
	
	@PostConstruct()
	public void initData() {

		Category tshirt = categoryService.saveInBase(new Category("T-shirt"));
		Category pull = categoryService.saveInBase(new Category("Pull"));
		Category legging = categoryService.saveInBase(new Category("Legging"));
		Category jogging = categoryService.saveInBase(new Category("Jogging"));
		Category shoes = categoryService.saveInBase(new Category("Chaussure"));
		Category equipment = categoryService.saveInBase(new Category("Equipement"));
		Category access = categoryService.saveInBase(new Category("Accessoire"));
		
		ProductPerso product1 = productPersoService.saveProductPerso(new ProductPerso(1,1,"T-shirt Rouge","Un très beau t-shirt rouge",8.0,"Michel Forever 06 66 66 66 66",tshirt,true));
	}

}
