package fr.afcepf.al34.ws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.ws.dao.CategoryDao;
import fr.afcepf.al34.ws.entity.Category;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao categoryDao;
	
	@Override
	public Category saveInBase(Category c) {
		return categoryDao.save(c);
	}

}
