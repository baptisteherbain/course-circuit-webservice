package fr.afcepf.al34.ws.entity;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor 
@Entity
@Table(name = "product_perso")
public class ProductPerso  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="idClient")
	private Integer idClient;

	private String name;

	private String description;

	private double price;
	
	private String commentaire;
	
	@ManyToOne
	@JoinColumn(name = "id_category")
	private Category category;
	
	private boolean vendu;

	public ProductPerso(Integer id, Integer idClient, String name, String description, double price, String commentaire, Category category, boolean vendu) {
		super();
		this.id = id;
		this.idClient = idClient;
		this.name = name;
		this.description = description;
		this.price = price;
		this.commentaire = commentaire;
		this.category = category;
		this.vendu = vendu;
	}
	
	
}
