package fr.afcepf.al34.ws.service;

import java.util.List;

import fr.afcepf.al34.ws.entity.ProductPerso;
import fr.afcepf.al34.ws.exception.MyEntityNotFoundException;

public interface ProductPersoService {
	
	ProductPerso saveProductPerso (ProductPerso product);
	void deleteProductPerso(Integer id) throws MyEntityNotFoundException;
	List<ProductPerso>findAll();
	List<ProductPerso>findByidClient(Integer idClient);
	

}
