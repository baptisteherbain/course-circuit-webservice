package fr.afcepf.al34.ws.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.ws.entity.Category;

public interface CategoryDao extends CrudRepository<Category,String> {

}
