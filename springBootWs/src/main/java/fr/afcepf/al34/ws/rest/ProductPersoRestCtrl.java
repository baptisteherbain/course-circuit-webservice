package fr.afcepf.al34.ws.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.ws.entity.ProductPerso;
import fr.afcepf.al34.ws.service.ProductPersoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/productPerso-api/public/productPerso" , headers="Accept=application/json")
@Api(tags = { "plop (public part)" })
@CrossOrigin("*") //"*" ou "www.ami1.com" , "www.ami2.com"
public class ProductPersoRestCtrl {

	
	@Autowired
	private ProductPersoService productPersoService;
	
	
	@ApiOperation(value= "getProductPersobyIdClient")
	@GetMapping(value="/{idClientProducPerso}")
	public List<ProductPerso> getProductPersoByIdClient(@PathVariable("idClientProductPerso") Integer idClient	) {
		return productPersoService.findByidClient(idClient);
		
	}
	@ApiOperation(value= "getAllProductPerso")
	@GetMapping(value="")
	public List<ProductPerso> getAllProductPerso() {
		return productPersoService.findAll();
		
	}
	@PostMapping(value="")
	public ProductPerso postProductPerso(@Valid @RequestBody ProductPerso productPerso) {
		return productPersoService.saveProductPerso(productPerso);
	}
}
