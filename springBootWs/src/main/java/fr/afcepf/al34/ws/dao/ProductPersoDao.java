package fr.afcepf.al34.ws.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.ws.entity.ProductPerso;

public interface ProductPersoDao extends CrudRepository<ProductPerso,Integer>{

	List<ProductPerso> findByIdClient(Integer idClient);
	
	
}
